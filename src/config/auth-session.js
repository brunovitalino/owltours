const uuid = require('uuid/v4');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const UserDAO = require('../app/api/infra/user-dao');
const pool = require('./database');

const userDAO = new UserDAO(pool);

module.exports = (app) => {

    passport.use(new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password'
        },
        (email, password, done) => {
            userDAO.findByEmail(email).then(data => {
                const user = data.rows[0];
                if (!user || user.password != password)
                    // done(error? / authenticated? / wht hppned?)
                    return done(null, false, {
                        mesage: 'Login and password incorrect'
                    });
                return done(null, user);
            }).catch(err => done(err, false));
        }
    ));

    passport.serializeUser((user, done) => {
        const userSession = {
            name: user.name,
            name: user.email
        };
        done(null, userSession);
    });

    passport.deserializeUser((userSession, done) => {
        done(null, userSession);
    });

    app.use(session({
        secret: 'node oowlish',
        genid: function(req) {
            return uuid();
        },
        resave: false,
        saveUninitialized: false
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    app.use(function(req, resp, next) {
        req.passport = passport;
        next();
    });
}