// POSTGRES
const pg = require('pg');
const config = {
    user: 'postgres',
    password: '123', 
    database: 'owltours',
    port: 5432, 
    max: 10, // max number of connection can be open to database
    idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};

// MAIN
const pool = new pg.Pool(config);

module.exports = function() {
    return new Promise((resolve, reject) => {
        pool.connect(function(err, client, done) {
            if(err) {
                let errMsg = "Not able to get connection.";
                console.log(errMsg + " ["+err+"]");
                reject(errMsg);
            }
            resolve(client);
            done();
        });
    });
}


