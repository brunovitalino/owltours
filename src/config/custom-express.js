require("marko/node-require"); // Allow Node.js to require and load `.marko` files
require("marko/express");

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const methodOverride = require('method-override')

app.use('/static', express.static('src/app/portal/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

const authSession = require('./auth-session');
authSession(app);

const apiRoutes = require('../app/api/routes/routes');
apiRoutes(app);
const viewRoutes = require('../app/portal/routes/routes');
viewRoutes(app);

module.exports = app;