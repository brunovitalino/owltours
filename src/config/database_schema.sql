-- DROP DATABASE
SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'owltours' AND pid <> pg_backend_pid();
DROP DATABASE owltours;

-- CREATE DATABASE
CREATE DATABASE owltours;

-- CREATE USERS
CREATE SEQUENCE "seq_users_id";
CREATE TABLE "public"."users" (
	"id" int NOT NULL DEFAULT nextval('seq_users_id'::regclass),
	"name" varchar(50) NOT NULL,
	"email" varchar(50) NOT NULL,
	"password" varchar(15) NOT NULL,
	"date_creation" date NOT NULL
);
ALTER TABLE "users" ADD CONSTRAINT "uniq_users_email" UNIQUE("email");
ALTER SEQUENCE "seq_users_id" OWNED BY "users"."id";
INSERT INTO "public"."users"("name", "email", "password", "date_creation") 
VALUES('Admin', 'admin@owltours.com', 123, CURRENT_DATE);

-- CREATE DRIVERS
CREATE TABLE "public"."drivers" (
	"name" varchar(50) NOT NULL,
	"id" int NOT NULL,
	"age" int NOT NULL,
	"date_creation" date NOT NULL
);
CREATE UNIQUE INDEX "PK_drivers" ON "public"."drivers"("id");
CREATE SEQUENCE "seq_drivers_id";
ALTER TABLE "drivers" ALTER COLUMN "id" SET DEFAULT nextval('seq_drivers_id'::regclass);
ALTER SEQUENCE "seq_drivers_id" OWNED BY "drivers"."id";

-- CREATE RIDES
CREATE TABLE "public"."rides"
(
  "id" int NOT NULL,
  "id_driver" int NOT NULL,
  "from" varchar(50) NOT NULL,
  "to" varchar(50) NOT NULL,
  "value" numeric NOT NULL,
  CONSTRAINT "fk_driver_id" FOREIGN KEY ("id_driver") REFERENCES "public"."drivers"("id")
);
CREATE INDEX "idx_driver_id" ON "public"."rides"("id_driver");
CREATE SEQUENCE "seq_rides_id";
ALTER TABLE "rides" ALTER COLUMN "id" SET DEFAULT nextval('seq_rides_id'::regclass);
ALTER SEQUENCE "seq_rides_id" OWNED BY "rides"."id";
