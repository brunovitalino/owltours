class UserDAO {

    constructor(pool) {
        this._db = pool;
    }

    all() {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'SELECT * FROM users ORDER BY id',
                    (err, result) => {
                        if(err) return reject(`Unable to display list of users. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    findById(id) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'SELECT * FROM users WHERE id = $1', 
                    [id], 
                    (err, result) => {
                        if(err) return reject(`Unable to display list of users. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    }

    findByEmail(email) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'SELECT * FROM users WHERE email = $1', 
                    [email], 
                    (err, result) => {
                        if(err) return reject(`Unable to display list of users. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    }

    save(user) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'INSERT INTO users(name, email, password, date_creation) values($1, $2, $3, CURRENT_DATE) RETURNING id',
                    [user.name, user.email, user.password],
                    (err, result) => {
                        if(err) return reject(`Unable to insert new user. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    update(user) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'UPDATE users SET name = $2, email = $3, password = $4 WHERE id = $1',
                    [user.id, user.name, user.email, user.password],
                    (err, result) => {
                        if(err) return reject(`Unable to update user. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    remove(id) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'DELETE FROM users WHERE id = $1', 
                    [id], 
                    (err, result) => {
                        if(err) {
                            const errMsg = `Unable to remove user of id "${id}". [${err}]`;
                            console.log(errMsg);
                            return reject(errMsg);
                        }
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    }
}

module.exports = UserDAO;