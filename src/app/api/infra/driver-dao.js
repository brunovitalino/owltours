class DriverDAO {

    constructor(pool) {
        this._db = pool;
    }

    all() {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'SELECT * FROM drivers ORDER BY id',
                    (err, result) => {
                        if(err) return reject(`Unable to display list of drivers. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    findById(id) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'SELECT * FROM drivers WHERE id = $1', 
                    [id], 
                    (err, result) => {
                        if(err) return reject(`Unable to display list of drivers. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    }

    save(driver) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'INSERT INTO drivers(name, age, date_creation) values($1, $2, CURRENT_DATE) RETURNING id',
                    [driver.name, driver.age],
                    (err, result) => {
                        if(err) return reject(`Unable to insert new driver. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    update(driver) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'UPDATE drivers SET name = $2, age = $3 WHERE id = $1',
                    [driver.id, driver.name, driver.age],
                    (err, result) => {
                        if(err) return reject(`Unable to update driver. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    remove(id) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'DELETE FROM rides WHERE id_driver = $1', 
                    [id], 
                    (err, result) => {
                        if(err) console.log(`Unable to remove rides bound to driver of id "${id}". [${err}]`);
                    }
                );
                client.query(
                    'DELETE FROM drivers WHERE id = $1', 
                    [id], 
                    (err, result) => {
                        if(err) {
                            const errMsg = `Unable to remove driver of id "${id}". [${err}]`;
                            console.log(errMsg);
                            return reject(errMsg);
                        }
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    }
}

module.exports = DriverDAO;