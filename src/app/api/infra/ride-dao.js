class RideDAO {

    constructor(pool) {
        this._db = pool;
    }

    all() {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'SELECT * FROM rides ORDER BY id',
                    (err, result) => {
                        if(err) return reject(`Unable to get list of rides. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    findById(id) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'SELECT * FROM rides WHERE id = $1', 
                    [id], 
                    (err, result) => {
                        if(err) return reject(`Unable to get list of rides. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    }

    save(ride) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'INSERT INTO rides(id_driver, "from", "to", "value") values($1, $2, $3, $4) RETURNING id',
                    [ride.id_driver, ride.from, ride.to, ride.value],
                    (err, result) => {
                        if(err) return reject(`Unable to insert new ride. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    update(ride) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'UPDATE rides SET id_driver = $2, "from" = $3, "to" = $4, "value" = $5 WHERE id = $1',
                    [ride.id, ride.id_driver, ride.from, ride.to, ride.value],
                    (err, result) => {
                        if(err) return reject(`Unable to update ride. [${err}]`);
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    };

    remove(id) {
        return new Promise((resolve, reject) => {
            this._db().then((client) => {
                client.query(
                    'DELETE FROM rides WHERE id = $1', 
                    [id], 
                    (err, result) => {
                        if(err) {
                            const errMsg = `Unable to remove ride of id "${id}". [${err}]`;
                            console.log(errMsg);
                            return reject(errMsg);
                        }
                        return resolve(result);
                    }
                );
            }).catch(dbErr => {
                return reject(dbErr)
            });
        });
    }
}

module.exports = RideDAO;