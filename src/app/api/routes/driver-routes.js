const Driver = require('../models/driver');
const DriverController = require('../controllers/driver-controller');

module.exports = (app) => {
    const driverController = new DriverController();
    const driverRoutes = DriverController.routes();
    
    app.route(driverRoutes.drivers)
        .get(driverController.all())
        .post(Driver.validations(), driverController.save());

    app.route(driverRoutes.driversWithId)
        .get(driverController.findById())
        .put(Driver.validations(), driverController.update())
        .delete(driverController.remove());
}

