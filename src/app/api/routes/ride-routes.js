const Ride = require('../models/ride');
const RideController = require('../controllers/ride-controller');

module.exports = (app) => {
    const rideController = new RideController();
    const rideRoutes = RideController.routes();
    
    app.route(rideRoutes.rides)
        .get(rideController.all())
        .post(Ride.validations(), rideController.save())
        .put(Ride.validations(), rideController.update())

    app.route(rideRoutes.ridesWithId)
        .get(rideController.findById())
        .delete(rideController.remove());
}

