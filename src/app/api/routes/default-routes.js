const DefaultController = require('../controllers/default-controller');

module.exports = (app) => {
    const defaultController = new DefaultController();
    const defaultRoutes = DefaultController.routes();

    app.get(defaultRoutes.status, defaultController.ok());
}