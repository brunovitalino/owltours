const defaultRoutes = require('./default-routes');
const driverRoutes = require('./driver-routes');
const rideRoutes = require('./ride-routes');

module.exports = (app) => {
    defaultRoutes(app);
    driverRoutes(app);
    rideRoutes(app);
}