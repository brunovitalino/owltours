const { validationResult } = require('express-validator');

const RideDAO = require('../infra/ride-dao');
const pool = require('../../../config/database');

const rideDAO = new RideDAO(pool);

class RideController {

    static routes() {
        const root = '/api/';

        return {
            rides: root + 'rides',
            ridesWithId: root + 'rides/:id'
        }
    }

    all() {
        return function(req, res) {
            rideDAO.all().then(data => 
                res.status(200).send(data.rows)
            ).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    findById() {
        return function(req, res) {
            rideDAO.findById(req.params.id).then(data => {
                if(data.rows<1) res.status(200).send({});
                res.status(200).send(data.rows[0]);
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    save() {
        return function(req, res) {
            // validation
            const validationErrors = validationResult(req);
            if (!validationErrors.isEmpty())
                return res.status(422).json({validationErrors: validationErrors.array()});
            // then save
            const ride = req.body;
            rideDAO.save(ride).then(data => {
                const createdId = data.rows[0].id;
                res.location(`/rides/${createdId}`);
                res.status(201).send();
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    update() {
        return function(req, res) {
            // validation
            const validationErrors = validationResult(req);
            if (!validationErrors.isEmpty())
                return res.status(422).json({validationErrors: validationErrors.array()});
            // then update
            const ride = req.body;
            rideDAO.update(ride).then(data => {
                // checks unaffected rows
                if(data.rowCount==0) res.status(409).send(`Id doesn't exists.`);
                res.status(204).send();
            }).catch(err => {
                console.log(err);
                res.status(500).send()
            });
        }
    }

    remove() {
        return function(req, res) {
            const id = req.params.id;
            rideDAO.remove(id).then(data => {
                // checks unaffected rows
                if(data.rowCount==0) res.status(409).send(`Id doesn't exists.`);
                res.status(204).send()
            }).catch(err => {
                if (err) {
                    console.log(err);
                    res.status(500).send();
                }
            });
        }
    }
}

module.exports = RideController;