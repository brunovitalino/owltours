const { validationResult } = require('express-validator');

const DriverDAO = require('../infra/driver-dao');
const pool = require('../../../config/database');

const driverDAO = new DriverDAO(pool);

class DriverController {

    static routes() {
        const root = '/api/';

        return {
            drivers: root + 'drivers',
            driversWithId: root + 'drivers/:id'
        }
    }

    all() {
        return function(req, res) {
            driverDAO.all().then(data => 
                res.status(200).send(data.rows)
            ).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    findById() {
        return function(req, res) {
            driverDAO.findById(req.params.id).then(data => {
                if(data.rows<1) res.status(200).send({});
                res.status(200).send(data.rows[0]);
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    save() {
        return function(req, res) {
            // validation - finds the validation errors in this request and wraps them in an object with handy functions
            const validationErrors = validationResult(req);
            if (!validationErrors.isEmpty())
                return res.status(422).json({validationErrors: validationErrors.array()});
            // then save
            const driver = req.body;
            driverDAO.save(driver).then(data => {
                const createdId = data.rows[0].id;
                res.location(`/drivers/${createdId}`);
                res.status(201).send();
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    update() {
        return function(req, res) {
            // validation
            const validationErrors = validationResult(req);
            if (!validationErrors.isEmpty())
                return res.status(422).json({validationErrors: validationErrors.array()});
            // then update
            const driver = req.body;
            driver.id = req.params.id;
            driverDAO.update(driver).then(data => {
                // checks unaffected rows
                if(data.rowCount==0) res.status(409).send(`Id doesn't exists.`);
                res.status(204).send();
            }).catch(err => {
                console.log(err);
                res.status(500).send()
            });
        }
    }

    remove() {
        return function(req, res) {
            const id = req.params.id;
            driverDAO.remove(id).then(data => {
                // checks unaffected rows
                if(data.rowCount==0) res.status(409).send(`Id doesn't exists.`);
                res.status(204).send()
            }).catch(err => {
                if (err) {
                    console.log(err);
                    res.status(500).send();
                }
            });
        }
    }
}

module.exports = DriverController;