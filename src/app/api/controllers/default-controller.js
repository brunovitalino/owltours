class DefaultController {

    static routes() {
        return {
            status: '/api/status'
        }
    }

    ok() {
        return function(req, res) {
            res.send('ok');
        }
    }
}

module.exports = DefaultController;