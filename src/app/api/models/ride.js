const { check } = require('express-validator');

class Ride {

    static validations() {
        return [
            check('id_driver').isInt().withMessage(`'id_driver' must be an integer.`),
            check('from').isLength({min: 2, max: 50}).withMessage(`'from' must be at minimum 2 and maximum 50 chars long.`),
            check('to').isLength({min: 2, max: 50}).withMessage(`'to' must be at minimum 2 and maximum 50 chars long.`),
            check('value').isCurrency().withMessage(`'value' must be a currency value.`)
        ]
    }
}

module.exports = Ride;