const { check } = require('express-validator');

class Driver {

    static validations() {
        return [
            check('name').isLength({min: 2, max: 50}).withMessage(`Name must be at minimum 2 and maximum 50 chars long.`),
            check('age').isNumeric().withMessage(`Age must be number.`)
        ]
    }
}

module.exports = Driver;