const { validationResult } = require('express-validator');
const DriverDAO = require('../../api/infra/driver-dao');
const pool = require('../../../config/database');
const template = require('../views/templates');

const driverDAO = new DriverDAO(pool);

class DriverController {

    static routes() {
        const root = '/';

        return {
            authenticateds: root + 'drivers*',
            list: root + 'drivers',
            form: root + 'drivers/form',
            formWithId: root + 'drivers/form/:id'
        }
    }

    list() {
        return function(req, res) {
            driverDAO.all().then(data =>
                res.marko(
                    template.drivers.list,
                    { drivers: data.rows }
                )
            ).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    formSave() {
        return function(req, res) {
            res.marko(template.drivers.form, { driver: {} });
        }
    }

    formEdit() {
        return function(req, res) {
            const driverId = req.params.id;
            driverDAO.findById(driverId).then(data => {
                res.marko(
                    template.drivers.form,
                    { driver: data.rows[0] }
                )
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    save() {
        return function(req, res) {
            const driver = req.body;
            // validation
            const validationErr = validationResult(req);
            if (!validationErr.isEmpty())
                return res.marko(
                    template.drivers.form,
                    {
                        driver: driver,
                        validationErrors: validationErr.array()
                    }
                );
            // then save
            driverDAO.save(driver).then(data => {
                res.redirect(DriverController.routes().list);
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    update() {
        return function(req, res) {
            const driver = req.body;
            // validation
            const validationErr = validationResult(req);
            if (!validationErr.isEmpty())
                return res.marko(
                    template.drivers.form,
                    {
                        driver: driver,
                        validationErrors: validationErr.array()
                    }
                );
            // then edit
            driverDAO.update(driver).then(data => {
                res.redirect(DriverController.routes().list);
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }
}

module.exports = DriverController;