const { validationResult } = require('express-validator');
const DriverDAO = require('../../api/infra/driver-dao');
const RideDAO = require('../../api/infra/ride-dao');
const pool = require('../../../config/database');
const template = require('../views/templates');

const driverDAO = new DriverDAO(pool);
const rideDAO = new RideDAO(pool);

class RideController {

    static routes() {
        const root = '/';

        return {
            authenticateds: root + 'rides*',
            list:           root + 'rides',
            form:           root + 'rides/form',
            formWithId:     root + 'rides/form/:id'
        }
    }

    list() {
        return function(req, res) {
            let drivers = [];
            let rides = [];

            driverDAO.all().then(data => {
                drivers = data.rows

                rideDAO.all().then(data => {
                    rides = data.rows;
                    let driver;
                
                    rides.forEach(ride => {
                        
                        ride.driver = drivers.find(function(driver) {
                            if(driver.id == ride.id_driver)
                                return driver;
                        });
                    });

                    res.marko(
                        template.rides.list,
                        { rides: data.rows }
                    )
                }).catch(err => {
                    console.log(err);
                    res.status(500).send();
                });                
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    formSave() {
        return function(req, res) {
            let drivers = {};

            driverDAO.all().then(data => {
                drivers = data.rows;
                
                res.marko(
                    template.rides.form,
                    { 
                        ride: {},
                        drivers: drivers
                    }
                );
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    formEdit() {
        return function(req, res) {
            const rideId = req.params.id;
            rideDAO.findById(rideId).then(data => {
                let ride = data.rows[0];
                let drivers = {};

                driverDAO.all().then(data => {
                    drivers = data.rows;
                    
                    res.marko(
                        template.rides.form,
                        { 
                            ride: ride,
                            drivers: drivers
                        }
                    )
                }).catch(err => {
                    console.log(err);
                    res.status(500).send();
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send();
            });
        }
    }

    save() {
        return function(req, res) {
            const ride = req.body;
            // validation
            const validationErr = validationResult(req);
            if (!validationErr.isEmpty()) {

                driverDAO.all().then(data => {
                    const drivers = data.rows;
                
                    return res.marko(
                        template.rides.form,
                        {
                            ride: ride,
                            validationErrors: validationErr.array(),
                            drivers: drivers
                        }
                    );
                }).catch(err => {
                    console.log(err);
                    return res.status(500).send();
                });
            }else
                // then save
                rideDAO.save(ride).then(data => {
                    res.redirect(RideController.routes().list);
                }).catch(err => {
                    console.log(err);
                    res.status(500).send();
                });
        }
    }

    update() {
        return function(req, res) {
            const ride = req.body;
            // validation
            const validationErr = validationResult(req);
            if (!validationErr.isEmpty()) {

                driverDAO.all().then(data => {
                    const drivers = data.rows;

                    return res.marko(
                        template.rides.form,
                        {
                            ride: ride,
                            validationErrors: validationErr.array(),
                            drivers: drivers
                        }
                    );
                }).catch(err => {
                    console.log(err);
                    return res.status(500).send();
                });
            }else
                // then edit
                rideDAO.update(ride).then(data => {
                    res.redirect(RideController.routes().list);
                }).catch(err => {
                    console.log(err);
                    res.status(500).send();
                });
        }
    }
}

module.exports = RideController;