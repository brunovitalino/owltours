const templates = require('../views/templates');

class DefaultController {

    static routes() {
        return {
            home: '/',
            login: '/login'
        }
    }

    homePage() {
        return function(req, res) {
            res.marko(templates.default.home);
        }
    }

    loginForm() {
        return function(req, res) {
            res.marko(templates.default.login, { user: {} });
        }
    }

    loginProcess() {
        return function(req, res, next) {
            // injection
            const passport = req.passport;

            passport.authenticate('local', (err, user, info) => {
                if (info) return res.marko(templates.default.login);
                if (err)  return next(err);
                req.login(user, (err) => {
                    if (err) return next(err);
                    res.redirect(DefaultController.routes().home);
                });
            })(req, res, next);
        }
    }

}

module.exports = DefaultController;