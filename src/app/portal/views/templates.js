module.exports = {
    // importing index.js from each folder
    default: require('./default'),
    drivers: require('./driver'),
    rides: require('./ride')
}