let ridesTable = document.querySelector('#ridesTable');
ridesTable.addEventListener('click', (event) => {
    let clickedElement = event.target;

    if (clickedElement.dataset.type == 'removal') {
        let rideId = clickedElement.dataset.ref;
        fetch(`http://localhost:3000/api/rides/${rideId}`, { method: 'DELETE' }).then(res => {
            let tr = clickedElement.closest(`#ride_${rideId}`);
            tr.remove();
        }).catch(err => console.log(err));
    }
});