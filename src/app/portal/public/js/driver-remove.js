let driversTable = document.querySelector('#driversTable');
driversTable.addEventListener('click', (event) => {
    let clickedElement = event.target;

    if (clickedElement.dataset.type == 'removal') {
        let driverId = clickedElement.dataset.ref;
        fetch(`http://localhost:3000/api/drivers/${driverId}`, { method: 'DELETE' }).then(res => {
            let tr = clickedElement.closest(`#driver_${driverId}`);
            tr.remove();
        }).catch(err => console.log(err));
    }
});