const RideController = require('../controllers/ride-controller');
const Ride = require('../models/ride');
const DefaultController = require('../controllers/default-controller');

module.exports = (app) => {
    const rideController = new RideController();
    const rideRoutes = RideController.routes();

    app.use(rideRoutes.authenticateds, function(req, res, next) {
        if (!req.isAuthenticated())
            res.redirect(DefaultController.routes().login);
        next();
    });

    app.get(rideRoutes.list, rideController.list()); // list

    app.route(rideRoutes.form)
        .get(rideController.formSave()) // form save
        .post(Ride.validations(), rideController.save()) // save
        .put(Ride.validations(), rideController.update()); // update

    app.get(rideRoutes.formWithId, rideController.formEdit()); // form edit
}