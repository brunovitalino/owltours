const DriverController = require('../controllers/driver-controller');
const Driver = require('../models/driver');
const DefaultController = require('../controllers/default-controller');

module.exports = (app) => {
    const driverController = new DriverController();
    const driverRoutes = DriverController.routes();

    app.use(driverRoutes.authenticateds, function(req, res, next) {
        if (!req.isAuthenticated())
            res.redirect(DefaultController.routes().login);
        next();
    });

    app.get(driverRoutes.list, driverController.list()); // list

    app.route(driverRoutes.form)
        .get(driverController.formSave()) // form save
        .post(Driver.validations(), driverController.save()) // save
        .put(Driver.validations(), driverController.update()); // update

    app.get(driverRoutes.formWithId, driverController.formEdit()); // form edit
}