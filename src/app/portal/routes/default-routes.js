const DefaultController = require('../controllers/default-controller');

module.exports = (app) => {
    const defaultController = new DefaultController();
    const defaultRoutes = DefaultController.routes(); 

    app.route(defaultRoutes.home)
        .get(defaultController.homePage());

    app.route(defaultRoutes.login)
        .get(defaultController.loginForm())
        .post(defaultController.loginProcess());
}
